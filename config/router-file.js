const filenames = {
    'notes/api/v1/rest': [
        'notes-resource'
    ],

    /**
   * Core REST.
   */
  'core/api/v1/rest': [
    'user-resource'
  ],

  /**
   * Core RPC.
   */
   'core/api/v1/rpc': [
    'auth-resource',
    'oauth-resource'
  ],
}

function toFilesList(imports, folder){
    return [...imports, ...filenames[folder].map(toRelativePaths(folder))]
}

function toRelativePaths(folder){
    return (filename) => {
        return `../src/module/${folder}/${filename}`
    }
}


module.exports = Object.keys(filenames).reduce(toFilesList, [])