const express = require("express")
const router = express.Router()
const service = require("../../../service/notes-service.js")
const model = require('../../../model')
router.get(process.env.BASE_URL+"/notes/api/v1/rest/notes",
    async function(req, resp){
        // const response = await service.getAll()
        let response = await model.notes.findAll()
        return resp.status(200).send(response)
})

router.post(process.env.BASE_URL+"/notes/api/v1/rest/notes",
    async function(req, resp){
        const data = req.body
        try {
            result = await model.notes.create(data)
            return resp.status(201).send(result)
        } catch (err){
            console.log(err.message)
            return resp.status(500).send({"error": err.message})    
        }
})


module.exports = router