'use strict';
module.exports = (sequelize, DataTypes) => {
  const Notes = sequelize.define('notes', {
    title: {
      type: DataTypes.STRING,
    },
    description: {
      type: DataTypes.STRING,
    },
    
  }, {
    tableName: 'notes',
    timestamps: true  
  });
  
  return Notes;
};